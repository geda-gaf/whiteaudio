# Dutch translation for the geda-gattrib package.
# Copyright (C) 2003-2008 Stuart D. Brorson and the respective original authors (which are listed on the respective files).
# This file is distributed under the same license as the geda-gattrib package.
# Bert Timmerman <bert.timmerman@xs4all.nl>, 2007, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: geda\n"
"Report-Msgid-Bugs-To: geda-bug@seul.org\n"
"POT-Creation-Date: 2011-06-14 00:12+0100\n"
"PO-Revision-Date: 2010-01-29 16:51+0000\n"
"Last-Translator: lambert63 <bert.timmerman@xs4all.nl>\n"
"Language-Team: gEDA developers <geda-dev@seul.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2010-02-14 12:56+0000\n"
"X-Generator: Launchpad (build Unknown)\n"
"X-Poedit-Country: NETHERLANDS\n"
"X-Poedit-Language: Dutch\n"

#: gattrib/data/geda-gattrib.desktop.in:3
msgid "gEDA Attribute Editor"
msgstr "gEDA Attribuut Bewerking"

#: gattrib/data/geda-gattrib.desktop.in:4
msgid "Manipulate component attributes with gattrib"
msgstr "Manipuleer componenten attributen met gattrib"
